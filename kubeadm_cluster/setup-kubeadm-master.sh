#!/usr/bin/env bash

set -e

# Ref: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

# Initialize controlplane
kubeadm init --cri-socket unix:///var/run/cri-dockerd.sock --pod-network-cidr=10.244.0.0/16

# Setup overlay network
export KUBECONFIG=/etc/kubernetes/admin.conf
echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> ~/.profile
kubectl apply -f https://github.com/flannel-io/flannel/releases/download/v0.24.0/kube-flannel.yml


# Generate Tokens and join workers to the cluster
# Discovery token
(openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //') 2>/dev/null > /root/discovery_token.txt
# Join token
kubeadm token create > /root/join_token.txt

declare joinToken=$(cat /root/join_token.txt)
declare discoveryToken=$(cat /root/discovery_token.txt)

ssh worker1 kubeadm join master:6443 --token "${joinToken}" --cri-socket unix:///var/run/cri-dockerd.sock --discovery-token-ca-cert-hash "sha256:${discoveryToken}"
ssh worker2 kubeadm join master:6443 --token "${joinToken}" --cri-socket unix:///var/run/cri-dockerd.sock --discovery-token-ca-cert-hash "sha256:${discoveryToken}"
ssh worker3 kubeadm join master:6443 --token "${joinToken}" --cri-socket unix:///var/run/cri-dockerd.sock --discovery-token-ca-cert-hash "sha256:${discoveryToken}"

# Configure Helm
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update


# Install ingress-nginx
# Ref: https://kubernetes.github.io/ingress-nginx/deploy/#quick-start
#helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/cloud/deploy.yaml

