#!/usr/bin/env bash

# Ref: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

set -e

# Run as root
apt-get update
apt-get -y install vim tmux curl net-tools jq yq
update-alternatives --set editor /usr/bin/vim.basic

cat > /etc/vim/vimrc <<EOF
runtime! debian.vim
let g:skip_defaults_vim = 1
syntax on
set background=dark
filetype plugin indent off
set showmatch
set ignorecase
set incsearch
set mouse-=a
EOF

apt-get -y install docker.io ethtool socat conntrack

tomlq  -t '.plugins."io.containerd.grpc.v1.cri".sandbox_image = "registry.k8s.io/pause:3.9"' /etc/containerd/config.toml > /tmp/config.toml
cp /tmp/config.toml /etc/containerd/config.toml
systemctl restart containerd

wget -nv -O /usr/local/bin/kubeadm https://dl.k8s.io/release/v1.29.0/bin/linux/amd64/kubeadm
wget -nv -O /usr/local/bin/kubelet https://dl.k8s.io/release/v1.29.0/bin/linux/amd64/kubelet
wget -nv -O /usr/local/bin/kubectl https://dl.k8s.io/release/v1.29.0/bin/linux/amd64/kubectl

chmod +x /usr/local/bin/kubeadm
chmod +x /usr/local/bin/kubelet
chmod +x /usr/local/bin/kubectl

wget -nv -O - "https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.29.0/crictl-v1.29.0-linux-amd64.tar.gz" | tar -C /usr/local/bin -xz
chown root:root /usr/local/bin/crictl

mkdir -p /opt/cni/bin
wget -nv -O - "https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz" | sudo tar -C "/opt/cni/bin" -xz


wget -nv https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.8/cri-dockerd_0.3.8.3-0.debian-bullseye_amd64.deb
dpkg -i cri-dockerd_0.3.8.3-0.debian-bullseye_amd64.deb
rm -f cri-dockerd_0.3.8.3-0.debian-bullseye_amd64.deb

wget -nv https://get.helm.sh/helm-v3.13.1-linux-amd64.tar.gz
tar xzf helm-v3.13.1-linux-amd64.tar.gz
cp linux-amd64/helm /usr/local/bin/
rm -rf ./linux-amd64 ./helm-v3.13.1-linux-amd64.tar.gz


cat > /etc/systemd/system/kubelet.service <<EOF
[Unit]
Description=kubelet: The Kubernetes Node Agent
Documentation=https://kubernetes.io/docs/
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=/usr/local/bin/kubelet
Restart=always
StartLimitInterval=0
RestartSec=10

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /etc/systemd/system/kubelet.service.d
cat > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf <<EOF
[Service]
Environment="KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf"
Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"
# This is a file that "kubeadm init" and "kubeadm join" generates at runtime, populating the KUBELET_KUBEADM_ARGS variable dynamically
EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env
# This is a file that the user can use for overrides of the kubelet args as a last resort. Preferably, the user should use
# the .NodeRegistration.KubeletExtraArgs object in the configuration files instead. KUBELET_EXTRA_ARGS should be sourced from this file.
EnvironmentFile=-/etc/sysconfig/kubelet
ExecStart=
ExecStart=/usr/local/bin/kubelet \$KUBELET_KUBECONFIG_ARGS \$KUBELET_CONFIG_ARGS \$KUBELET_KUBEADM_ARGS \$KUBELET_EXTRA_ARGS
EOF

systemctl enable kubelet --now

cat >> ~/.bashrc << EOF
alias k=kubectl
alias knodes='kubectl get nodes -o wide'
alias kpods='kubectl get pods -o wide'
alias kevents='kubectl get events --sort-by=".lastTimestamp"'
alias kall='kubectl api-resources --verbs list --namespaced -o name | args -n 1 kubectl get --show-kind --ignore-not-found'
source <(kubectl completion bash)
complete -o default -F __start_kubectl k
EOF

