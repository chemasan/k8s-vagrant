# Generate a user certificate
openssl genrsa -out myuser.key 2048
openssl req -new -key myuser.key -out myuser.csr -subj "/CN=myuser/O=developers/O=operators"

declare csr=$(base64 myuser.csr | tr -d '\n')

kubectl apply -f - << EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: myuser
spec:
  request: ${csr}
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 63072000  # 2y. k8s may limit of 1y
  usages:
  - client auth
EOF

kubectl get csr myuser
kubectl certificate approve myuser
kubectl get csr myuser

declare crt=$(kubectl get csr myuser -o yaml | yq -r .status.certificate)
echo ${crt} | base64 -d | tee myuser.crt

# Generate kube-config
declare ca=$(base64 /etc/kubernetes/pki/ca.crt | tr -d '\n')
declare key=$(base64 myuser.key | tr -d '\n')

tee myuser-kubeconfig.conf << EOF
apiVersion: v1
kind: Config
clusters:
- name: kubernetes
  cluster:
    certificate-authority-data: ${ca}
    server: https://192.168.121.42:6443
users:
- name: myuser
  user:
    client-certificate-data: ${crt}
    client-key-data: ${key}
contexts:
- name: myuser@kubernetes
  context:
    cluster: kubernetes
    user: myuser
preferences: {}
current-context: myuser@kubernetes
EOF

# Grant permissions
kubectl apply -f - << EOF
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: myuser-granted-view
subjects:
- kind: User
  name: myuser
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io
EOF
